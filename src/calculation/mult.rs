use crate::{Currency, CurrencyLocale};
use std::ops::Mul;

impl<L: CurrencyLocale + Default> Mul<usize> for Currency<L> {
    type Output = Self;

    fn mul(self, rhs: usize) -> Self::Output {
        Self::new(
            self.negative,
            (self.amount as f64 * rhs as f64) as usize,
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Mul<Currency<L>> for usize {
    type Output = Currency<L>;

    fn mul(self, rhs: Currency<L>) -> Self::Output {
        rhs * self
    }
}

impl<L: CurrencyLocale + Default> Mul<Currency<L>> for isize {
    type Output = Currency<L>;

    fn mul(self, rhs: Currency<L>) -> Self::Output {
        rhs * self
    }
}

impl<L: CurrencyLocale + Default> Mul<isize> for Currency<L> {
    type Output = Self;

    fn mul(self, rhs: isize) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_negative(),
            self.amount * rhs.unsigned_abs(),
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Mul<f32> for Currency<L> {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_sign_negative(),
            (self.amount * (rhs * 100.0).abs() as usize) / 100,
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Mul<Currency<L>> for f32 {
    type Output = Currency<L>;

    fn mul(self, rhs: Currency<L>) -> Self::Output {
        rhs * self
    }
}

impl<L: CurrencyLocale + Default> Mul<f64> for Currency<L> {
    type Output = Self;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_sign_negative(),
            (self.amount * (rhs * 100.0).abs() as usize) / 100,
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Mul<Currency<L>> for f64 {
    type Output = Currency<L>;

    fn mul(self, rhs: Currency<L>) -> Self::Output {
        rhs * self
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Clone, Copy, Default, Debug, PartialEq)]
    enum CurrencyL {
        #[default]
        De,
    }

    impl CurrencyLocale for CurrencyL {
        fn separator(&self) -> char {
            ','
        }

        fn thousand_separator(&self) -> char {
            '.'
        }

        fn currency_symbol(&self) -> &'static str {
            "€"
        }
    }

    #[test]
    fn mul_usize() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 2usize;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_usize_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = 2usize * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_isize() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 2isize;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_isize_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = 2isize * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_isize_negative() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * -2isize;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_isize_negative_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = -2isize * curr;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_isize_negative() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr * -2isize;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_isize_negative_rev() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = -2isize * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 2.0f32;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_f32_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = 2.0f32 * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_f32_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = 2.0f32 * curr;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_f32_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = -2.0f32 * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 0.5f32;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_negative_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * -0.5f32;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_f32_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr * 0.5f32;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_negative_fraction_f32_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr * -0.5f32;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 2.0f64;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * -2.0f64;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_f64_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = 2.0f64 * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_f64_rev() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = -2.0f64 * curr;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_f64_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = 2.0f64 * curr;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_negative_f64_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = -2.0f64 * curr;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * 0.5f64;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_negative_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr * -0.5f64;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_fraction_f64_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr * 0.5f64;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn mul_negative_fraction_f64_negative_currency() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr * -0.5f64;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }
}
