use crate::{Currency, CurrencyLocale};
use std::ops::Div;

impl<L: CurrencyLocale> Div<usize> for Currency<L> {
    type Output = Self;

    fn div(self, rhs: usize) -> Self::Output {
        Self::new(self.negative, self.amount / rhs, self.locale)
    }
}

impl<L: CurrencyLocale> Div<isize> for Currency<L> {
    type Output = Self;

    fn div(self, rhs: isize) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_negative(),
            self.amount / rhs.unsigned_abs(),
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Div<f32> for Currency<L> {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_sign_negative(),
            // multiply by 100 to prevent casting to floats
            self.amount * 100 / (rhs * 100.0).abs() as usize,
            self.locale,
        )
    }
}

impl<L: CurrencyLocale + Default> Div<f64> for Currency<L> {
    type Output = Self;

    fn div(self, rhs: f64) -> Self::Output {
        Self::new(
            self.negative ^ rhs.is_sign_negative(),
            // multiply by 100 to prevent casting to floats
            self.amount * 100 / (rhs * 100.0).abs() as usize,
            self.locale,
        )
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Clone, Copy, Default, Debug, PartialEq)]
    enum CurrencyL {
        #[default]
        De,
    }

    impl CurrencyLocale for CurrencyL {
        fn separator(&self) -> char {
            ','
        }

        fn thousand_separator(&self) -> char {
            '.'
        }

        fn currency_symbol(&self) -> &'static str {
            "€"
        }
    }

    #[test]
    fn div_usize() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 2usize;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_isize() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 2isize;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_isize_negative() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / -2isize;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_negative_isize_negative() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr / -2isize;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_negative_isize() {
        let curr = Currency::new(true, 2_22, CurrencyL::De);

        let expected = curr / 2isize;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 2.0f32;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_negative_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / -2.0f32;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_fraction_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 0.5f32;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn div_fraction_negative_f32() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / -0.5f32;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }

    #[test]
    fn div_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 2.0f64;
        assert_eq!(expected, Currency::new(false, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_negative_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / -2.0f64;
        assert_eq!(expected, Currency::new(true, 1_11, CurrencyL::De));
    }

    #[test]
    fn div_fraction_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / 0.5f64;
        assert_eq!(expected, Currency::new(false, 4_44, CurrencyL::De));
    }

    #[test]
    fn div_fraction_negative_f64() {
        let curr = Currency::new(false, 2_22, CurrencyL::De);

        let expected = curr / -0.5f64;
        assert_eq!(expected, Currency::new(true, 4_44, CurrencyL::De));
    }
}
