# null-kane

This is a Rust crate designed for currency manipulation. It provides an
interface via a trait for implementing localization methods. The trait enables
retrieval of the currency separator, thousand separator, and currency symbol.

## Features

### Localization Methods

- `get_separator`: Retrieves the separator used for the currency.
- `get_thousand_separator`: Retrieves the thousand separator used for the currency.
- `get_currency_symbol`: Retrieves the currency symbol.

### Arithmetic Operations

The crate implements arithmetic operations for the following types:
- Addition, multiplication, and division among currencies.
- Addition, multiplication, and division between currencies and `usize`.
- Addition, multiplication, and division between currencies and `f32`.
- Addition, multiplication, and division between currencies and `f64`.

### Constructors

`From` implementations for creating instances from `f32` and `f64`.

## Example

```rust
use null_kane::{Currency, CurrencyLocale};

#[derive(Clone, Default, PartialEq, Eq)]
enum MyLocalization {
    #[default]
    Euro
}

impl CurrencyLocale for MyLocalization {
    fn separator(&self) -> char {
        // Implementation of the separator for Euro
        ','
    }

    fn thousand_separator(&self) -> char {
        // Implementation of the thousand separator for Euro
        '.'
    }

    fn currency_symbol(&self) -> &'static str {
        // Implementation of the currency symbol for Euro
        "€"
    }
}

fn main() {
    let euro_currency = MyLocalization::default();
    let amount_one = Currency::from(50.25_f32).with_locale(euro_currency.clone());
    let amount_two = Currency::from(30.75_f64).with_locale(euro_currency);

    let sum = amount_one + amount_two;
    let product = amount_one * 2.0;
    let division = amount_two / 3.0;

    println!("Sum: {}", sum);
    println!("Product: {}", product);
    println!("Division: {}", division);
}
```

## TODO

- implement a good solution for failing operations, for example if currency localizations don't match
- consider using num trait


# License

This project is licensed under the MIT License. See the [LICENSE](/LICENSE.md) file for details.
